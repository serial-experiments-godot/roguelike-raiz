extends MapEntity

export (int) var attack_damage = 10

onready var aim = $Aim

var aim_coords: Vector2

func _ready():
	._ready()
	move(Vector2.ZERO)


func _input(event):
	_movement_input(event)
	
	if event.is_action_pressed("attack"):
		_attack()


func _get_move_direction(event) -> Vector2:
	var vertical_movement = 0
	var horizontal_movement = 0
	
	if event.is_action_pressed("move_forward"):
		vertical_movement = -1
	elif event.is_action_pressed("move_back"):
		vertical_movement = 1
	
	if event.is_action_pressed("move_left"):
		horizontal_movement = -1
	elif event.is_action_pressed("move_right"):
		horizontal_movement = 1
	
	return Vector2(horizontal_movement, vertical_movement)


func _update_sprite_look(direction: Vector2):
	._update_sprite_look(direction)
	if sprite.flip_h:
		aim.position.x = -2 * tile_size_offset.x
	else:
		aim.position.x = 2 * tile_size_offset.x


func _go_to_map_coords(coords: Vector2):
	._go_to_map_coords(coords)
	aim_coords = current_coords + (Vector2(-1,0) if sprite.flip_h else Vector2(1,0))


func _movement_input(event):
		
	var move_dir := _get_move_direction(event)
	
	if move_dir == Vector2.ZERO:
		return
	
	move(move_dir)


func _attack():
	var target = map.get_target_at(aim_coords) as MapEntity
	
	if target == null:
		return
	
	target.receive_damage(attack_damage)
