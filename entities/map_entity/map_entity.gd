extends Node2D
class_name MapEntity

export (Vector2) var starting_position
export (int) var starting_life

onready var sprite = $Sprite
onready var animation_player = $AnimationPlayer

var map: TileMap
var tile_size_offset: Vector2
var current_coords: Vector2
var current_life: int


func _ready():
	tile_size_offset = map.cell_size / 2
	_go_to_map_coords(starting_position)
	current_life = starting_life


func _go_to_map_coords(coords:Vector2):
	position = map.map_to_world(coords) + tile_size_offset
	current_coords = map.world_to_map(position)


func _can_move_to(direction: Vector2):
	var current_position = map.world_to_map(position)
	var target_tile  = map.get_cellv(current_position + direction)
	var cell_shapes = map.tile_set.tile_get_shapes(target_tile)
	return cell_shapes.empty() and not map.OCCUPIED_TILES.has(current_position + direction)


func _update_sprite_look(direction: Vector2):
	
	if direction.y != 0:
		return
	
	sprite.flip_h = direction.x == -1


func move(to):
	_update_sprite_look(to)
	
	if not _can_move_to(to):
		return
	
	var target_coors = current_coords + to	
	
	_go_to_map_coords(target_coors)


func receive_damage(amount):
	current_life -= amount
	animation_player.play("take_damage")
