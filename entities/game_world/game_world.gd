extends TileMap

const OCCUPIED_TILES = {}

var enemies

func _enter_tree():
	enemies = $Enemies
	
	for enemy in enemies.get_children():
		enemy.map = self
		OCCUPIED_TILES[enemy.starting_position] = enemy
	
	print(OCCUPIED_TILES)


func get_target_at(coord: Vector2):
	return OCCUPIED_TILES.get(coord)
